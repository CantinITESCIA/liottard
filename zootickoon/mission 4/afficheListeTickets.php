<?php
$mysqli = new mysqli("localhost", "root", "", "ticket");
$requete = "SELECT * FROM ticket";
$resultat = $mysqli -> query($requete);
while ($ligne = $resultat -> fetch_assoc()) {
	echo <<<MON_HTML
	<html>
  <head>
    <title>Les différents Ticket Zoo</title>
    <meta charset='utf-8'>
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  </head>
  <body>
    <div class="container">
      <h1 class="text-center">Les différents Ticket Zoo</h1>
      <table class="table table-dark">
        <thead>
          <tr>
            <th>Login</th>
            <th>Sujet</th>
			<th>Description</th>
			<th>Prio</th>
			<th>Secteur</th>
			<th>Statut</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>${ligne['login']}</td>
            <td>${ligne['sujet']}</td>
			<td>${ligne['description']}</td>
			<td>${ligne['prio']}</td>
            <td>${ligne['secteur']}</td>
            <td>${ligne['statut']}</td>
          </tr>
        </tbody>
      </table>
    </div>
  </body>
</html>
MON_HTML;
	}
	$mysqli->close();
?>